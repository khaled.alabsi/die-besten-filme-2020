package com.alabsi.BeliebteFilme4U.FilmPhotos;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FilmPhotosApplication {

	public static void main(String[] args) {
		SpringApplication.run(FilmPhotosApplication.class, args);
	}

}
