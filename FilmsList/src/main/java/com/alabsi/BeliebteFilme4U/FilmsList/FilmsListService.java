package com.alabsi.BeliebteFilme4U.FilmsList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class FilmsListService {

    @Autowired
    private RestTemplate restTemplate;
    private Map<Integer,String> filmsList = new HashMap<>();

    @Autowired
    Chaos chaos;

    public ConatainerOfListFilmModel getFilmsList() {
        chaos.startChaos();
        List<FilmListModel> list = new ArrayList<>( );
        for (Map.Entry<Integer, String> entry : filmsList.entrySet()) {

            FilmListModel filmListModel = new FilmListModel(entry.getKey(),entry.getValue(),restTemplate.getForObject("http://localhost:8083/description?FilmId="+entry.getKey(), String.class));
            list.add(filmListModel);
        }

        return  new ConatainerOfListFilmModel(list);

    }

    FilmsListService(){
        filmsList.put(1,"TENET");
        filmsList.put(2,"DER UNSICHTBARE");
        filmsList.put(3,"I'M THINKING OF ENDING THINGS");
        filmsList.put(4,"TYLER RAKE: EXTRACTION");
        filmsList.put(5,"BERLIN ALEXANDERPLATZ");
        filmsList.put(6,"UNDINE");
        filmsList.put(7,"NIEMALS SELTEN MANCHMAL IMMER");

    }

}
