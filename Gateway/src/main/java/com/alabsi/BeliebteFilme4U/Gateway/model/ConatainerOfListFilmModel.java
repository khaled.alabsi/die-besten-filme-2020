package com.alabsi.BeliebteFilme4U.Gateway.model;

import java.util.ArrayList;
import java.util.List;

public class ConatainerOfListFilmModel {

    List<FilmListModel> list = new ArrayList<>();

    public ConatainerOfListFilmModel(List<FilmListModel> list) {
        this.list = list;
    }

    public List<FilmListModel> getList() {
        return list;
    }

    public void setList(List<FilmListModel> list) {
        this.list = list;
    }

    public ConatainerOfListFilmModel() {
    }
}
