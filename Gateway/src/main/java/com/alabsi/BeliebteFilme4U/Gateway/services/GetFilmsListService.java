package com.alabsi.BeliebteFilme4U.Gateway.services;

import com.alabsi.BeliebteFilme4U.Gateway.model.AllFilmsListModel;

import com.alabsi.BeliebteFilme4U.Gateway.model.ConatainerOfListFilmModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.List;

@Service
public class GetFilmsListService {

    @Autowired
    private RestTemplate restTemplate;

    Logger logger= LoggerFactory.getLogger(GetFilmsListService.class);
    public List<AllFilmsListModel> getFilmslist(){

        List<AllFilmsListModel> list = new ArrayList<>();
       ConatainerOfListFilmModel cn =  restTemplate.getForObject("http://localhost:8082/filmslist", ConatainerOfListFilmModel.class);
        cn.getList().stream().forEach(x->{
            logger.info(String.valueOf(x));
            AllFilmsListModel film = new AllFilmsListModel();
            film.setId(x.getId());
            film.setName(x.getName());
            film.setDescription(x.getDescription());
            film.setFotoUrl(restTemplate.getForObject("http://localhost:8081/PhotoUrl?FilmId="+x.getId(), String.class));
            film.setRating(restTemplate.getForObject("http://localhost:8084/filmrating?FilmId="+x.getId(), Double.class));
            list.add(film);
        });

        List<AllFilmsListModel> filmListModel = new ArrayList<AllFilmsListModel>();
        AllFilmsListModel fl1 = new AllFilmsListModel();
        fl1.setId(1);
        fl1.setName("TENET");
        fl1.setDescription("Actionfilm von Christopher Nolan mit John David Washington und Robert Pattinson. In Christopher Nolans Thriller Tenet wird John David Washington in eine Welt der Spionage hineingezogen, die er sich so niemals hätte ausmalen können, da die Regeln der Zeit hier scheinbar anders funktionieren.");
        //fl1.setFotoUrl("https://assets.cdn.moviepilot.de/files/ef2e68275e326e1181183c6fa57207af8893115bc76b5ea1a0f3e4d86f8e/fill/155/223/DE_Hauptplakat_TENET.jpg");
        fl1.setRating(7.3);
        filmListModel.add(fl1);
        fl1.setFotoUrl(restTemplate.getForObject("http://localhost:8081/PhotoUrl?FilmId=1", String.class));

        AllFilmsListModel fl2 = new AllFilmsListModel();
        fl2.setId(2);
        fl2.setName("Der Unsichbar");
        fl2.setDescription("Horrorfilm von Leigh Whannell mit Elisabeth Moss und Aldis Hodge. Der Unsichtbare verfolgt in diesem Psychohorrorfilm Elisabeth Moss, als sie glaubt, sich endlich von ihrem gewalttätigen Ehemann losgesagt zu haben.");
        fl2.setFotoUrl("https://assets.cdn.moviepilot.de/files/b912977439dca15fe6c4fa7b36944a2b28d278090e64d0c6c93b268389c4/fill/155/223/der_unsichtbare_plakat.jpg");
        fl2.setRating(7.1);
        filmListModel.add(fl2);


        filmListModel.add(fl2);
        filmListModel.add(fl2);
        filmListModel.add(fl2);

        return list;

    }

}


