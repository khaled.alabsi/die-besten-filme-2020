package com.alabsi.BeliebteFilme4U.FilmsList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class FilmsListController {

    @Autowired
    FilmsListService filmsListService;

    @GetMapping("/filmslist")
        public ConatainerOfListFilmModel getUrl(){

        return filmsListService.getFilmsList();
    }


}
