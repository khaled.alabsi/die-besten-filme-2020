package com.alabsi.BeliebteFilme4U.FilmRating;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

@Service
public class FilmRatingService {

    @Autowired
    Chaos chaos;

    Map<Integer,Double> rating = new HashMap<>( );


    public Double getFilmRating(int id){
        chaos.startChaos();

      return rating.containsKey(id)? rating.get(id) : 0.0 ;
    };

    public FilmRatingService() {

        rating.put(1,7.3);
        rating.put(2,7.1);
        rating.put(3,7.5);
        rating.put(4,6.3);
        rating.put(5,7.1);
        rating.put(6,7.3);
        rating.put(7,7.9);
    }
}
