package com.alabsi.BeliebteFilme4U.AboutFilm;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

@Service
public class AboutFilmService {

    Map<Integer, String> description = new HashMap<>();
    @Autowired
    Chaos chaos;
    public String getFilmDescription(int id){
        chaos.startChaos();
        if(description.containsKey((id))){
            return description.get(id);
        }else {
            return " keine Infos";
        }

    }
    public AboutFilmService() {

        description.put(1, "Actionfilm von Christopher Nolan mit John David Washington und Robert Pattinson. In Christopher Nolans Thriller Tenet wird John David Washington in eine Welt der Spionage hineingezogen, die er sich so niemals hätte ausmalen können, da die Regeln der Zeit hier scheinbar anders funktionieren.");
        description.put(2, "Horrorfilm von Leigh Whannell mit Elisabeth Moss und Aldis Hodge. Der Unsichtbare verfolgt in diesem Psychohorrorfilm Elisabeth Moss, als sie glaubt, sich endlich von ihrem gewalttätigen Ehemann losgesagt zu haben.");
        description.put(3, "Psychothriller von Charlie Kaufman mit Jessie Buckley und Jesse Plemons. In Netflix' Psychothriller-Romanverfilmung I'm Thinking of Ending Things von Charlie Kaufman beginnt Jessie Buckley ihre Beziehung und die Welt bei einem Besuch der Schwiegereltern in Frage zu stellen.");
        description.put(4, "Drama von Sam Hargrave mit Chris Hemsworth und David Harbour. Tyler Rake: Extraction ist ein Action-Thriller, den die Russo-Brüder Joe und Anthony für Netflix produzieren. Chris Hemsworth spielt einen abgehärteten Kopfgeldjäger, der angeheuert wird, den Sohn eines hochrangigen, international bekannten Mafiabosses zu retten.");
        description.put(5, "Drama von Burhan Qurbani mit Welket Bungué und Jella Haase. In der modernen Adaption Berlin Alexanderplatz des gleichnamigen Romans von Alfred Döblin versucht ein afrikanischer Flüchtling sich in Deutschland auf anständigem Wege durchzuschlagen, wird aber immer wieder kriminell in Versuchung geführt.");
        description.put(6, "Drama von Christian Petzold mit Paula Beer und Franz Rogowski. Im Liebesdrama Undine wird Paula Beer von ihrem Freund verlassen, wodurch ein Fluch ans Licht kommt, der sie dazu zwingt, ihn zu ertränken. Doch dann verliebt die Nixe sich in einen Taucher.");
        description.put(7, "Drama von Eliza Hittman mit Sidney Flanigan und Talia Ryder. Im Drama Never Rarely Sometimes Always reisen zwei Teenagerinnen nach New York, um medizinische Hilfe nach einer ungeplanten Schwangerschaft zu erhalten.");
    }
}
