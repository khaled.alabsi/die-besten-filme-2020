package com.alabsi.BeliebteFilme4U.Gateway.controller;

import com.alabsi.BeliebteFilme4U.Gateway.model.AllFilmsListModel;

import com.alabsi.BeliebteFilme4U.Gateway.services.GetFilmsListService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@Controller
@RequestMapping(value = "/")

public class HomeController {

    @Autowired
    private GetFilmsListService FilmeServie;

    @RequestMapping("/")
    public List<AllFilmsListModel> home() {
        return FilmeServie.getFilmslist();
    }

    @RequestMapping("/list")
    public String spaceships(Model model) {
        model.addAttribute("films", FilmeServie.getFilmslist());
        return "FilmsList";
    }
}
