package com.alabsi.BeliebteFilme4U.FilmsList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
@Component
public class Chaos {

    Logger logger = LoggerFactory.getLogger(Chaos.class);

    @Value(value = "${chaos.props.secondsToSleep:0}")
    int secondsToSleep;

    @Value(value = "${chaos.props.randSleep:false}")
    boolean randSleep;

    @Value(value = "${chaos.props.randError:false}")
    boolean randCrash;

    @Value(value = "${chaos.props.error:false}")
    boolean crash;

    @Value(value = "${chaos.props.randShutdown:false}")
    boolean randShutdown;

    @Value(value = "${chaos.props.randThreadOverload:false}")
    boolean randThreadOverload;

    @Value(value = "${chaos.props.randAll:false}")
    boolean randAll;

    @Value(value = "${chaos.props.howRand:0.06}")
    float howRand;


    @Value(value = "${chaos.props.overloadRate:50}")
    int overloadRate;
    @Value(value = "${chaos.props.alwaysSleep:false}")
    boolean alwaysSleep;

    @Autowired
    private ApplicationContext appContext;


    public void startChaos(){
        logger.warn("starting Chaos!");
        if(randSleep | randAll){
            double x = Math.random();
            if( x < 0.5*howRand ){
                logger.warn("sleeping rand = "+x);
                try {
                    Thread.sleep(secondsToSleep * 1000);
                } catch (InterruptedException ie) {
                    Thread.currentThread().interrupt();
                }
            }

        }

        if(alwaysSleep) {
            try {
                Thread.sleep(secondsToSleep * 1000);
            } catch (InterruptedException ie) {
                Thread.currentThread().interrupt();
            }

        }

        if(randCrash | randAll){
            if( Math.random() < 0.5*howRand ){
                logger.warn("crash");
                int x= 5/0;
            }

        }

        if(crash ){
            int x= 5/0;
        }

        if(randShutdown | randAll){
            if( Math.random() < 0.5*howRand ) {
                SpringApplication.exit(appContext, () -> -1);
            }
        }
        if(randThreadOverload | randAll) {
            if (Math.random() < 0.5*howRand) {
                for (int i = 0; i < overloadRate; i++) {
                    NewThread thread = new NewThread();
                    thread.run();
                }
            }
        }

    }

    public class NewThread extends Thread {
        @Async
        public void run() {
            long startTime = System.currentTimeMillis();
            double i = 0;
            while (i < 99.99999999 ) {
                logger.warn(this.getName() + ": New Thread is running..." + (i=i+0.1));
                try {
                    //Wait for one sec so it doesn't print too fast
                    Thread.sleep(1);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

            }
        }
    }

}
