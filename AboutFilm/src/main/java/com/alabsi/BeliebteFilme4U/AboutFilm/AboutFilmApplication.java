package com.alabsi.BeliebteFilme4U.AboutFilm;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AboutFilmApplication {

	public static void main(String[] args) {
		SpringApplication.run(AboutFilmApplication.class, args);
	}

}
