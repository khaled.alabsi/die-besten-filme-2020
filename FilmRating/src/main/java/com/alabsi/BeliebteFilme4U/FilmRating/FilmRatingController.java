package com.alabsi.BeliebteFilme4U.FilmRating;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class FilmRatingController {

    @Autowired
    FilmRatingService filmRatingService;

    @GetMapping("/filmrating")

    public double rating(@RequestParam int FilmId){

        return filmRatingService.getFilmRating(FilmId);
    }

}
