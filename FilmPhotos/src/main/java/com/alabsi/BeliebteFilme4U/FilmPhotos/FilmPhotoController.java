package com.alabsi.BeliebteFilme4U.FilmPhotos;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
public class FilmPhotoController {
    @Autowired
    FilmPhotoService filmPhotoService;

    @GetMapping("/PhotoUrl")
    @ResponseBody
    public String getUrl(@RequestParam int FilmId){

        return filmPhotoService.getFilmPhotoURLforThisID(FilmId);
    }

}

