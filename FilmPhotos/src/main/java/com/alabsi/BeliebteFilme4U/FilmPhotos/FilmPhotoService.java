package com.alabsi.BeliebteFilme4U.FilmPhotos;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.HashMap;
import java.util.Map;

@Service
public class FilmPhotoService {

    private Map<Integer,String> urlMap = new HashMap<>();

    @Autowired
     Chaos chaos;

     public String getFilmPhotoURLforThisID(int id){

         chaos.startChaos();

         if(urlMap.containsKey(id)){

             return urlMap.get(id);
         }else{
             return "https://image.shutterstock.com/image-vector/no-image-available-sign-absence-260nw-373243873.jpg";
         }


     }

FilmPhotoService(){
    urlMap.put(1,"https://assets.cdn.moviepilot.de/files/ef2e68275e326e1181183c6fa57207af8893115bc76b5ea1a0f3e4d86f8e/fill/155/223/DE_Hauptplakat_TENET.jpg");
    urlMap.put(2,"https://assets.cdn.moviepilot.de/files/b912977439dca15fe6c4fa7b36944a2b28d278090e64d0c6c93b268389c4/fill/155/223/der_unsichtbare_plakat.jpg");
    urlMap.put(3,"https://assets.cdn.moviepilot.de/files/f6995f5da43b16700d18c6d5a06930199d2d490d73b9502c0b251ce99638/fill/155/223/im_thinking_of_ending_things_xlg.jpg");
    urlMap.put(4,"https://assets.cdn.moviepilot.de/files/bd9a86da118d034f142e459295b98f35caa779f644496570610027d2ca43/fill/155/223/extraction_netflix_xxlg.jpg");
    urlMap.put(5,"https://assets.cdn.moviepilot.de/files/6ead89cc9fde8cdc06d78411e7327a3e3490008fe4772e556d8882f98155/fill/155/223/BerlinAlexanderplatz_Poster_RGB_HQ_A4.jpg");
    urlMap.put(6,"https://assets.cdn.moviepilot.de/files/59c1b6e400ddf4b4876b6aa6194b48dd1564fcd26a6d2def401fd3dacb9d/fill/155/223/Undine_Plakat_Web.jpg");
    urlMap.put(7,"https://assets.cdn.moviepilot.de/files/eba3fd504b029a4d5cad6fce37e6293b9c9372209164ae1f071551c3248d/fill/155/223/Niemals-Selten-Manchmal-Immer-A3.jpg");
}



}


