package com.alabsi.BeliebteFilme4U.AboutFilm;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class AboutFilmController {
    
   @Autowired
   AboutFilmService aboutFilmService;
   
   @GetMapping("/description")
    public String getDescription(@RequestParam int FilmId){
       return aboutFilmService.getFilmDescription(FilmId);
   }
}
